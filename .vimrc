" VIM-PLUG PLUGINS (WITH FIRST-RUN AUTOINSTALL)
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !mkdir -p "$HOME/.vim/autoload"; U="https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim";  F="$HOME/.vim/autoload/plug.vim";  curl -fLo "$F" "$U" 2>/dev/null  ||  wget -qLO "$F" "$U"
  autocmd VimEnter * PlugInstall
endif
call plug#begin()

" Default is 60
let g:plug_timeout = 600
" Default: 16
let g:plug_threads = 32

let macvim_skip_colorscheme=1

" Another reason to use iTerm 2 instead of Terminal.app
" Terminus enhances Vim's integration with the terminal in four ways.
" 1. Cursor shape change in insert mode
" 2. Improved mouse support
" 3. Focus reporting
" 4. Bracketed Paste mode
" Plug 'wincent/terminus'

Plug 'scrooloose/nerdtree'
let NERDTreeShowHidden=1
let g:NERDTreeWinSize=40
autocmd BufWritePre *.js :%s/\s\+$//e
autocmd vimenter * NERDTree %
autocmd VimEnter * wincmd p
autocmd BufEnter * lcd %:p:h
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
autocmd BufNewFile,BufReadPost *.md set filetype=markdown
au BufRead,BufNewFile .{eslintrc,babelrc,jscsrc} setf json
au! BufRead,BufNewFile *.json set filetype=json
au! BufRead,BufNewFile *.json set filetype=json nospell

" Auto-reload vimrc
augroup reload_vimrc " {
    autocmd!
    autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END " }

Plug 'Valloric/YouCompleteMe', { 'do': './install.sh' }

Plug 'elzr/vim-json', { 'for': ['json'] }
let g:vim_json_warnings = 1

Plug 'marijnh/tern_for_vim', { 'for': ['javascript'] }

Plug 'pangloss/vim-javascript', { 'for': ['javascript'] }

" auto-insert end of your block
Plug 'tpope/vim-endwise', { 'for': ['javascript'] }

Plug 'flazz/vim-colorschemes'
Plug 'wavded/vim-stylus', { 'for': ['stylus'] }

Plug 'digitaltoad/vim-jade', { 'for': ['jade'] }

Plug 'wincent/ferret'

" Use gc in visual mode to comment-out
Plug 'tpope/vim-commentary'

Plug 'othree/html5.vim', { 'for': ['html'] }

" Add to this as I dev in more proglangs
Plug 'scrooloose/syntastic', { 'for': ['javascript', 'java', 'groovy', 'html', 'css', 'c', 'ruby'] }
Plug 'mtscout6/syntastic-local-eslint.vim'
let g:syntastic_html_tidy_ignore_errors=[" proprietary attribute \"contenteditable", " proprietary attribute \"touch-action", " proprietary attribute \"webkit-playsinline"]
let g:syntastic_javascript_checkers=['eslint', 'jscs']
let g:syntastic_check_on_open=1
let g:syntastic_aggregate_errors=1
let g:syntastic_html_validator_parser = 'html5'
let g:syntastic_html_tidy_exec = 'tidy5'
" let g:syntastic_javascript_jshint_conf = $HOME . '/.jshintrc'
" let g:syntastic_debug = 3

" JSX Nonsense ;)
" let g:syntastic_javascript_checkers = ['jsxhint']
" let g:syntastic_javascript_jsxhint_exec = 'jsx-jshint-wrapper'
" let g:jsx_ext_required = 0

Plug 'bling/vim-airline'

" Unused for now
" Plug 'tpope/vim-fugitive'
" Plug 'mileszs/ack.vim'
" Plug 'tfnico/vim-gradle'
" Plug 'tyok/nerdtree-ack'
" Plug 'mxw/vim-jsx', { 'for': ['javascript'] }
" Plug 'ruanyl/vim-fixmyjs'
" Plug 'akhaku/vim-java-unused-imports'
" Plug 'gabrielelana/vim-markdown'
" This Vim plugin will search for terms using the excellent Dash.app,
" making API lookups simple.
" Plug 'rizzatti/dash.vim'

Plug 'jimmyhchan/dustjs.vim'

" Alignment & Tidyness
Plug 'godlygeek/tabular'
Plug 'ntpeters/vim-better-whitespace'
let g:strip_whitespace_on_save = 1
let g:better_whitespace_filetypes_blacklist=['markdown']

" https://github.com/sidorares/node-vim-debugger
Plug 'sidorares/node-vim-debugger', { 'do': 'cd ~/.vim/plugged/node-vim-debugger/ && npm install' }  " nodejs debugger
command! NodeDebug call conque_term#open('node ' . expand('~/.vim/plugged/node-vim-debugger/bin/vim-inspector') . ' "' . expand('%') . '"', ['belowright split'])


call plug#end()


" More tidyness
autocmd FileType javascript,css,python,ruby autocmd BufWritePre <buffer> StripWhitespace
highlight ExtraWhitespace ctermbg=yellow

" variables
autocmd CursorMoved * exe printf('match IncSearch /\V\<%s\>/', escape(expand('<cword>'), '/\'))

" colourlovers
colorscheme twilight256
set background=dark
set nu
set backspace=indent,eol,start
syntax on
set tabstop=2
set shiftwidth=2
set expandtab
nnoremap <F2> :set invpaste paste?<CR>
nnoremap <Leader>s :%s/\<<C-r><C-w>\>//g<Left><Left>
nnoremap <Leader>n :%s/\<<C-r><C-w>\>//gn<CR>
set pastetoggle=<F2>
set showmode
set autoindent
set cindent
set smartindent
set directory=$HOME/.vim/swapfiles//
set cursorline
set cursorcolumn
set shortmess=a
set cmdheight=2

let g:NERDTreeBookmarksFile = '/Users/jwomack/.vim/bundle/nerdtree/bookmarks'
let g:javascript_plugin_jsdoc = 1
let g:javascript_plugin_flow = 1
let g:tern_show_argument_hints = 1

"set spell spelllang=en_us

" keep more context when scrolling
set scrolloff=999
set sidescrolloff=5
set sidescroll=1

" default encoding
set encoding=utf-8

" To highlight all search matches, set the following option
set hlsearch

" keep 10000 lines of command line history
set history=10000

let jshint2_read = 1
let jshint2_save = 1

